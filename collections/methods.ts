import {Places} from './places';

function getContactEmail(user: Meteor.User): string {
    if (user.emails && user.emails.length)
        return user.emails[0].address;

    return null;
};

Meteor.methods({
    invite: function(placeId: string, userId: string) {
        check(placeId, String);
        check(userId, String);

        let place = Places.findOne(placeId);

        if (!place)
            //throw new Meteor.Error('404', 'No such place!');
            Materialize.toast('Ошибка 404: Место не найдено', 3000, 'red darken-4');
        if (place.public)
            //throw new Meteor.Error('400', 'That place is public. No need to ask people about it.');
            Materialize.toast('Ошибка 400: Это место известно всем', 3000, 'red darken-4');
        if (place.owner !== this.userId)
            Materialize.toast('Ошибка 403: Нет доступа', 3000, 'red darken-4');

        if (userId !== place.owner && (place.invited || []).indexOf(userId) == -1) {
            Places.update(placeId, { $addToSet: { invited: userId } });

            let from = getContactEmail(Meteor.users.findOne(this.userId));
            let to = getContactEmail(Meteor.users.findOne(userId));

            if (Meteor.isServer && to) {
                Email.send({
                    from: 'noreply@socially.com',
                    to: to,
                    replyTo: from || undefined,
                    subject: 'PLACE: ' + place.name,
                    text: `Hi, I just asked you about ${place.name}. Have you been there?
                        \n\nCome check it out: ${Meteor.absoluteUrl()}\n`
                });
            }
        }
    },
    reply: function(placeId: string, rsvp: string) {
        check(placeId, String);
        check(rsvp, String);

        if (!this.userId)
            //throw new Meteor.Error('403', 'You must be logged-in to reply');
            Materialize.toast('Ошибка 403: Вы не авторизированы!', 3000, 'red darken-4');
        if (['yes', 'no', 'maybe'].indexOf(rsvp) === -1)
            //throw new Meteor.Error('400', 'Invalid RSVP');
            Materialize.toast('Ошибка 400: Неверный RSVP', 3000, 'red darken-4');
        let place = Places.findOne({_id: placeId});

        if (!place)
            //throw new Meteor.Error('404', 'No such place');
            Materialize.toast('Ошибка 404: Место не найдено', 3000, 'red darken-4');
        if (place.owner === this.userId)
            //throw new Meteor.Error('500', 'You are the owner!');
            Materialize.toast('Ошибка 500: Вы являетесь владельцем', 3000, 'red darken-4');
        if (!place.public && (!place.invited || place.invited.indexOf(this.userId) == -1))
            //throw new Meteor.Error('403', 'No such place'); //
            Materialize.toast('Ошибка 403: Место не найдено', 3000, 'red darken-4');
        let rsvpIndex = place.rsvps ? place.rsvps.findIndex((rsvp) => rsvp.userId === this.userId) : -1;

        if (rsvpIndex !== -1) {
            // update existing rsvp entry
            if (Meteor.isServer) {
                // update the appropriate rsvp entry with $
                Places.update(
                    {_id: placeId, 'rsvps.userId': this.userId},
                    {$set: {'rsvps.$.response': rsvp}});
            } else {
                // minimongo doesn't yet support $ in modifier. as a temporary
                // workaround, make a modifier that uses an index. this is
                // safe on the client since there's only one thread.
                let modifier = {$set: {}};
                modifier.$set['rsvps.' + rsvpIndex + '.response'] = rsvp;

                Places.update(placeId, modifier);
            }
        } else {
            // add new rsvp entry
            Places.update(placeId,
                {$push: {rsvps: {userId: this.userId, response: rsvp}}});
        }

    }
});