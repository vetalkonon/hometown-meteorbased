import 'collections/places';
import './places';
import './users';
import 'collections/methods';

import {loadPlaces} from './load_places';
Meteor.startup(loadPlaces);
