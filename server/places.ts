import {Places} from 'collections/places';

function buildQuery(placeId: string, location: string): Object {
  var isAvailable = {
    $or: [
      { public: true },
      {
        $and: [
          { owner: this.userId },
          { owner: { $exists: true } }
        ],
      },
      {
        $and: [
          { invited: this.userId },
          { invited: { $exists: true } }
        ]
      }
    ]
  };

  if (placeId) {
    return { $and: [{ _id: placeId }, isAvailable] };
  }

  let searchRegEx = { '$regex': '.*' + (location || '') + '.*', '$options': 'i' };
  return { $and: [{ location: searchRegEx }, isAvailable] };

  return isAvailable;
}
Meteor.publish('places', function(options: Object, location: string) {
  Counts.publish(this, 'numberOfPlaces',
      Places.find(buildQuery.call(this, null, location)), { noReady: true });
  return Places.find(buildQuery.call(this, null, location), options);
});

Meteor.publish('place', function(placeId: string) {
  return Places.find(buildQuery.call(this, placeId));
});
