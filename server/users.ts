import {Places} from 'collections/places';

Meteor.publish('uninvited', function(placeId: string) {
    let place = Places.findOne(placeId);

    if (!place)
        throw new Meteor.Error('404', 'No such place!');

    return Meteor.users.find({
        _id: {
            $nin: place.invited || [],
            $ne: this.userId
        }
    });
});