import {bootstrap} from 'angular2-meteor';
import {AccountsUI} from 'meteor-accounts-ui';
import {PlacesList} from 'client/templates/js/places';
import {SearchPage} from 'client/templates/js/search';
import {Component, View, NgZone, provide, enableProdMode} from 'angular2/core';
import {PlaceDetails} from 'client/templates/js/place-details';
import {ROUTER_PROVIDERS, ROUTER_DIRECTIVES, RouteConfig, APP_BASE_HREF} from 'angular2/router';
import 'collections/methods';

Accounts.ui.config({
    passwordSignupFields: 'USERNAME_AND_EMAIL'
});

@Component({
    selector: 'app'
})
@View({
    directives: [ROUTER_DIRECTIVES, AccountsUI],
    templateUrl: 'client/templates/html/navbar.html'
})
@RouteConfig([
    { path: '/', as: 'PlacesList', component: PlacesList },
    { path: '/place/:placeId', as: 'PlaceDetails', component: PlaceDetails },
    { path: '/search', as: 'SearchPage', component: SearchPage }
])
class Hometown {
 }
// enableDevMode();
bootstrap(Hometown,[ROUTER_PROVIDERS, provide(APP_BASE_HREF, { useValue: '/' })]);
