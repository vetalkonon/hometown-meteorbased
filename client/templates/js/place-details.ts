import {Component, View} from 'angular2/core';
import {RouteParams} from 'angular2/router';
import {RouterLink} from 'angular2/router';
import {Places} from 'collections/places';
import {PlacesForm} from 'client/templates/js/places-form';
import {RequireUser} from 'meteor-accounts';
import {MeteorComponent} from 'angular2-meteor';
import {DisplayName} from 'lib/pipes';

@Component({
  selector: 'place-details'
})
@View({
  pipes: [DisplayName],
  directives: [RouterLink, PlacesForm],
  templateUrl: 'client/templates/html/place-details.html'
})
@RequireUser()
export class PlaceDetails extends MeteorComponent {
  place: Place;
  users: Mongo.Cursor<Object>;
  constructor(params: RouteParams) {
    super();
    var placeId = params.get('placeId');
    this.subscribe('place', placeId, () => {
      this.autorun(() => {
        this.place = Places.findOne(placeId);
        this.getUsers(this.place);
      },   true);
    });

    this.subscribe('uninvited', placeId, () => {
      this.getUsers(this.place);
    }, true);
  }

  getUsers(place: Place) {
    if (place) {
      this.users = Meteor.users.find({
        _id: {
          $nin: place.invited || [],
          $ne: Meteor.userId()
        }
      });
    }
  }
  savePlace(place) {
    if (Meteor.userId()) {
      Places.update(place._id, {
        $set: {
          name: place.name,
          description: place.description,
          location: place.location
        }
      });//Places Update
      Materialize.toast('Изменения сохранены!', 3000, 'green');
    } else {
      Materialize.toast('Ошибка: Вы не авторизированы!', 3000, 'red darken-4');
    }
  }
  invite(user: Meteor.User) {
    this.call('invite', this.place._id, user._id, (error) => {
      if (error) {
        Materialize.toast(`Не получилось спросить из-за ${error}`, 3000, 'green');
        return;
      }
      Materialize.toast(`Вопрос успешно отправлен`, 3000, 'green');
    });
  }
  reply(rsvp: string) {
    this.call('reply', this.place._id, rsvp, (error) => {
      if (error) {
        Materialize.toast(`Не получилось ответить из-за ${error}`, 3000, 'green');
      }
      else {
        Materialize.toast('Ответ успешно отправлен!', 3000, 'green');
      }
    });
  }
}
