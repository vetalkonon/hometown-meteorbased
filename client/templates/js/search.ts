import {Component, View, NgZone} from 'angular2/core';
import {RouterLink} from 'angular2/router';


@Component({
  selector: 'search'
})
@View({
  directives: [RouterLink],
  templateUrl: 'client/templates/html/search.html'
})

export class SearchPage {
}
