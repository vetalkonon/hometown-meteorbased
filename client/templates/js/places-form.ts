import {Component, View} from 'angular2/core';
import {FormBuilder, Control, ControlGroup, Validators} from 'angular2/common';
import {PlacesList} from 'client/templates/js/places';
import {Places} from 'collections/places';
import {MeteorComponent} from 'angular2-meteor';
import {AccountsUI} from 'meteor-accounts-ui';
import {InjectUser} from 'meteor-accounts'

@Component({
  selector: 'places-form'
})
@View({
  templateUrl: 'client/templates/html/places-form.html',
  directives: [AccountsUI]
})
@InjectUser()
export class PlacesForm extends MeteorComponent {
  placesForm: ControlGroup;
  constructor() {
    super();
    var fb = new FormBuilder();
    this.placesForm = fb.group({
      name: ['', Validators.required],
      description: [''],
      location: ['', Validators.required],
      public: [false]
    });
  }
  addPlace(place) {
    if (Meteor.userId()) {
      if (this.placesForm.valid) {
        Places.insert({
          name: place.name,
          description: place.description,
          location: place.location,
          public: place.public,
          owner: Meteor.userId()
        });
        (<Control>this.placesForm.controls['name']).updateValue('');
        (<Control>this.placesForm.controls['description']).updateValue('');
        (<Control>this.placesForm.controls['location']).updateValue('');
        (<Control>this.placesForm.controls['public']).updateValue(false);
      }
    } else {
      Materialize.toast('Ошибка: Вы не авторизированы!', 3000, 'red darken-4');
    }
  }//Place Constructor
}
