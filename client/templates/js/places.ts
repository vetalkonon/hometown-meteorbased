// export var Places = new Mongo.Collection('places');
import {Places} from 'collections/places';
import {Component, View, NgZone} from 'angular2/core';
import {PlacesForm} from 'client/templates/js/places-form';
import {MeteorComponent} from 'angular2-meteor';
import {RouterLink} from 'angular2/router';
import {AccountsUI} from 'meteor-accounts-ui';
import {InjectUser} from 'meteor-accounts';
import {PaginationService, PaginatePipe, PaginationControlsCpm} from 'ng2-pagination';
import {RsvpPipe} from 'lib/pipes';

@Component({
    selector: 'places',
    viewProviders: [PaginationService]
})
@View({
    directives: [PlacesForm, RouterLink, AccountsUI, PaginationControlsCpm],
    templateUrl: 'client/templates/html/places.html',
    pipes: [PaginatePipe, RsvpPipe]
})
@InjectUser()
export class PlacesList extends MeteorComponent {
    places:Mongo.Cursor<Object>;
    pageSize:number = 10;
    curPage:ReactiveVar<number> = new ReactiveVar<number>(1);
    nameOrder: ReactiveVar<number> = new ReactiveVar<number>(1);
    placesSize: number = 0;
    location: ReactiveVar<string> = new ReactiveVar<string>(null);

    constructor() {
        super();
        this.autorun(() => {
            let options = {
                limit: this.pageSize,
                skip: (this.curPage.get() - 1) * this.pageSize,
                sort: { name: this.nameOrder.get() }
            };
            this.subscribe('places', options, this.location.get(), () => {
                this.places = Places.find({}, { sort: { name: this.nameOrder.get() } });
            }, true);
        });
        this.autorun(() => {
            this.placesSize = Counts.get('numberOfPlaces');
        }, true);
    }

    removePlace(place) {
        if (Meteor.userId()) {
            Places.remove(place._id);
        } else {
            Materialize.toast('Ошибка: Вы не авторизированы!', 3000, 'red darken-4');
        }
    }

    search(value:string) {
        this.curPage.set(1);
        this.location.set(value);
    }
    onPageChanged(page: number) {
        this.curPage.set(page);
    }
    changeSortOrder(nameOrder: string) {
        this.nameOrder.set(parseInt(nameOrder));
    }
}
